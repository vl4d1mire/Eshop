/**
 * get data of Products from shop-data.json
 */
export async function getProducts() {
    try {
        const res = await fetch('/shop-data.json')
        const products = res.json()
        return products
    } catch (e) {
        console.log(e)
    }
}
