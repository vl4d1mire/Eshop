import React from "react";
import CatalogFilter from "../catalog-filter/catalog-filter.component";
import ProductsCart from "../product-list/product-list.component";
import Pagination from "../pagination/pagination.component";
import './catalog.styles.scss'

const Catalog = () => {
  return (
      <section className="catalog">
          <CatalogFilter/>
          <ProductsCart/>
          <Pagination/>
      </section>
  )
}

export default Catalog