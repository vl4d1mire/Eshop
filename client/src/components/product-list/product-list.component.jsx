import React, { useEffect } from 'react'
import Cart from '../cart/cart.component'

import {receivedProducts} from '../../redux/products/productsSlice'
import {useDispatch, useSelector} from 'react-redux';
import {getProducts} from '../../api/api';

import './product-list.styles.scss'

const ProductList = () => {

    const dispatch = useDispatch()
    const products = useSelector(state => state?.products.products)

    useEffect(() => {
        getProducts().then((data) => {
            dispatch(receivedProducts(data))
        })
    }, [dispatch])

    return (
        <div className="catalog__cart-container">
            <div className="products__cart cart">
                <div className="cart__container">
                    {
                        Object.values(products).map((item) => (
                        <Cart key={item.id} item={item}/>))
                    }
                </div>
            </div>
        </div>
    )
}

export default ProductList