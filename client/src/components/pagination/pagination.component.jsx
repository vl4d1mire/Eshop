import React from 'react'
import {NavLink} from 'react-router-dom'
import './pagination.styles.scss'

const Pagination = () => {

    return (
        <div className="catalog__pagination pagination">
            <div className="pagination-wrap">
                <div className="pagination__arrow pagination__arrow_left">
                    <NavLink to={'/'}><img alt="prevArrow" src="/images/icons/arrow_left.svg"/></NavLink>
                </div>
                <div className="pagination__numbers">
                    <NavLink to={'/'} className="active">1</NavLink>
                    <NavLink to={'/'}>2</NavLink>
                    <NavLink to={'/'}>3</NavLink>
                    <NavLink to={'/'}>4</NavLink>
                    <NavLink to={'/'}>5</NavLink>
                    <NavLink to={'/'}>6</NavLink>
                    <NavLink className="pagination__numbers-dots" to={'/'}>.....</NavLink>
                    <NavLink className="" to={'/'}>20</NavLink>
                </div>
                <div className="pagination__arrow pagination__arrow_right">
                    <NavLink to={'/'}><img alt="nextArrow" src="/images/icons/arrow_right.svg"/></NavLink>
                </div>
            </div>
        </div>
    )
}

export default Pagination